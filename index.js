var print_test = require('./kiosk-printer-test')

var text = "hello \n\n\n\n\n\n"

async function PrintTest(){
  const res = await print_test.Print(text)
}

async function PrintQr(){
  const res = await print_test.PrintQr("https://www.youtube.com/watch?v=RIFBoS04ETk")
}

async function PrintBarcode(){
  const res = await print_test.PrintBarcode('111222333444', 'EAN13', { width: 80, height: 100, position: 0, font: 'A'})
}

async function PartialCut(){
  const res = await print_test.PartialCut()
}

async function FullCut(){
  const res = await print_test.FullCut()
}


async function PrintImage(){
  const res = await print_test.PrintImage("./image.jpg", "image/jpg", 'd24')
}

PrintBarcode()
PartialCut()

PrintQr()
PartialCut()

PrintTest()
FullCut()

PrintImage()

