const Printer = require('escpos');
const escpos = require('escpos');
escpos.USB = require('escpos-usb');
const device  = new escpos.USB();
const options = { encoding: "GB18030" /* default */ }
const printer = new escpos.Printer(device, options);

RollPaperSensorStatus = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      printer.getStatus('RollPaperSensorStatus', function (data) {
        resolve(data)
      }).close()
    })
  })
}

OfflineCauseStatus = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      printer.getStatus('OfflineCauseStatus', function (data) {
        resolve(data)
      }).close()
    })
  })
}

PrinterStatus = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      printer.getStatus('PrinterStatus', function (data) {
        resolve(data)
      }).close()
    })
  })
}

ErrorCauseStatus = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error){
        reject(error)
      }
      printer.getStatus('ErrorCauseStatus', function (data) {
        resolve(data)
      }).close()
    })
  })
}

Print = function(text){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      device.open(function(error){
        if (error){
          reject(error)
          return
        }
        printer
          .align('lt') // lt -> left text, ct -> center text,  rt -> right text
          .font('A') // font A, B, C 
          .text(text)
          .close(function(){
            resolve('Print OK')
          })
      });
    })
  })
}

Println = function(text){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error){
        reject(error)
      }
      device.open(function(error){
        if (error){
          reject(error)
          return
        }
        printer
          .align('lt')
          .font('A') // font A, B, C 
          .text(text + "\n")
          .close(function(){
            resolve('Println OK')
          })
      });
    })
  })
}

FullCut = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }else{
        device.open(function(error_device){
          if (error_device){
            reject (error_device)  
            return
          }else{
            printer
              .align('lt')
              .font('A')
              .cut()
              .close(function(){
                resolve('FullCut OK')
              })
          }
        });
      }
    })
  })
}

PartialCut = function(){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      device.open(function(error){
        if (error) {
          reject (error)  
          return        
        }
        printer
          .align('lt')
          .font('A')
          .cut('PAPER_PART_CUT')
          .close()
      });
    })
  })
}

PrintQr = function(content) {
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      device.open(function(error){
        if (error) {
          reject (error)  
          return        
        }
        printer
          .align('lt')
          .font('A')
          .qrimage(content, function(err){
            this.cut()
            this.close(function(){
              resolve('PrintQr OK')
            });
          })
      });
    })
  })
}
/**
 * options = {
 *    width, height, position, font
 * }
 * type = EAN13, EAN8
 */
PrintBarcode = function (content, type, options){
  return new Promise((resolve, reject) => {
    device.open(function(error){
      if (error) {
        reject (error)  
        return        
      }
      printer
        .align('lt')
        .font("A")
        .barcode(content, type, options)
    })
  })
}

/**
 * Print image
 * @param {String} path_image - path image : example "./image.jpg", "./image.png"
 * @param {String} type - type image : example "image/jpg", "image/png"
 * @param {String} density - ความละเอียด : example "s8", "d8", "s24", "d24"
 * @return Promise with result obj
 * 
 */

PrintImage = function (path_image, type, density) {
  return new Promise((resolve, reject) => {
    escpos.Image.load(path_image, type, function(Image_){
      device.open(function(error){
        if (error) {
          reject (error)  
          return        
        }
        printer                                                                                                                                                                                                                            // //       .align('ct')
          .image(Image_, density)
          .then((data) => {        
            resolve(data)
        });
      });
    })
  })
}

module.exports = {
  RollPaperSensorStatus,
  OfflineCauseStatus,
  ErrorCauseStatus,
  PrinterStatus,
  Print,
  Println,
  FullCut,
  PartialCut,
  PrintQr,
  PrintBarcode,
  PrintImage
};
